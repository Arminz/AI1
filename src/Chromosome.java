import java.util.*;

/**
 * Created by armin on 3/28/17.
 */
public class Chromosome {
    private int[] genes = new int[26];
    private int fitnessWeight;
    public Chromosome(int[] genes)
    {
        System.arraycopy(genes,0,this.genes,0,26);
        fitnessWeight = calculateFitnessWeight();
    }

    public Chromosome(Chromosome parent1, Chromosome parent2,String crossoverType)
    {
        if (Objects.equals(crossoverType, Keys.TWO_POINT)) {
            Random random = new Random();
            int start = random.nextInt(Keys.CROSSOVER_POINT1_RANGE);
            int end = 25 - random.nextInt(Keys.CROSSOVER_POINT_2_RANGE);
            if (end < start) {
                int k = start;
                start = end;
                end = k;
            }
            int myGenes[] = new int[26];
            for (int i = 0; i < 26; i++)
                myGenes[i] = -1;
            for (int i = start; i <= end; i++)
                myGenes[i] = parent1.getGene(i);

            //        System.out.println(start + " | " + end);
            int parent2Iterator = 0;
            int childIterator = 0;
            while (childIterator < 26) {
                if (childIterator >= start && childIterator <= end)
                    childIterator = end + 1;
                if (childIterator >= 26)
                    break;
                boolean flag = false;
                for (int j = 0; j < 26; j++)
                    if (myGenes[j] == parent2.getGene(parent2Iterator)) {
                        //                    System.out.println("dare be ga mire " +childIterator + ":" + myGenes[j] + ":" + parent2.getGene(parent2Iterator));
                        flag = true;
                    }
                if (!flag) {
                    myGenes[childIterator] = parent2.getGene(parent2Iterator);
                    childIterator++;
                }
                parent2Iterator++;
            }

            this.genes = myGenes;
            fitnessWeight = calculateFitnessWeight();
        }
        else if (Objects.equals(crossoverType, Keys.K_POINT)) {


            for (int i = 0; i < 26; i++)
                this.genes[i] = -1;
            ArrayList<Integer> permutation = new ArrayList<>();
            for (int i = 0; i < 26; i++)
                permutation.add(i);
            Collections.shuffle(permutation);
            for (int i = 0; i < Keys.K_SELECT; i++)
                genes[permutation.get(i)] = parent1.getGene(permutation.get(i));


            int parent2Iterator = 0;
            ArrayList<Integer> parent2RemainedGenes = new ArrayList<>();
            for (int i = 0; i < 26; i++) {
                int thisGene = parent2.getGene(i);
                boolean flag = false;
                for (int j = 0; j < 26; j++)
                    if (genes[j] == thisGene)
                        flag = true;
                if (!flag)
                    parent2RemainedGenes.add(thisGene);
            }
            for (int i = 0; i < 26; i++)
                if (genes[i] == -1) {
                    genes[i] = parent2RemainedGenes.get(parent2Iterator);
                    parent2Iterator++;
                }

            fitnessWeight = calculateFitnessWeight();
        }
    }
    public Chromosome(Chromosome chromosome,int swapTime)
    {
        int myGenes[] = new int[26];
        for (int i = 0; i < 26; i++)
            myGenes[i] = chromosome.getGene(i);
        Random random = new Random();
        for (int h = 0; h < swapTime; h ++) {
            int first = random.nextInt(26);
            int second = random.nextInt(26);
            int k = myGenes[first];
            myGenes[first] = myGenes[second];
            myGenes[second] = k;
        }
        this.genes = myGenes;
        fitnessWeight = calculateFitnessWeight();
    }

    public Chromosome(ArrayList<Integer> permutation) {
        for (int i = 0 ; i < permutation.size() ; i ++)
            this.genes[i] = permutation.get(i);
        fitnessWeight = calculateFitnessWeight();
    }

    private int decodeIndex(int index)
    {
        if (index < 0 || index >= 26)
            throw new RuntimeException("index out of range [0:26)");
        for (int i = 0; i < 26 ; i ++)
            if (genes[i] == index)
                return i;
        throw new RuntimeException("Genes is not permutation");

    }
    public int getGene(int index)
    {
        return genes[index];
    }
    public String decodeString()
    {
        String code = Controller.getCode();
        String decodedString = "";
        for (int i = 0 ; i < code.length(); i ++)
        {
            if (code.charAt(i) >='A' && code.charAt(i) <= 'Z') {
                int charIndex = (code.charAt(i) - 'A');
                decodedString += (char) (decodeIndex(charIndex) + 'A');
            }
            else
                decodedString += code.charAt(i);
        }
        return decodedString;
    }

    private int calculateFitnessWeight()
    {
        FitnessWeightTable fitnessWeightTable = FitnessWeightTable.getInstance();
        String decodedString = decodeString();
        int fitnessWeight = 0;
        for (int i = 0 ; i < decodedString.length() ; i ++)
            for (int j = 0 ; j < fitnessWeightTable.getSize(); j ++)
                for (int k = 0 ; k < fitnessWeightTable.getText(j).length() && i+k < decodedString.length(); k ++){
//                    System.out.println(String.format("%s : %s" ,k,fitnessWeightTable.getText(j).length()));
                    if (fitnessWeightTable.getText(j).charAt(k) != decodedString.charAt(i + k)) {
//                        System.out.println(fitnessWeightTable.getText(j) + ": " + k + " != " + decodedString.charAt(i + k));
                        break;
                    }
                    if (k == fitnessWeightTable.getText(j).length() - 1) {
//                        System.out.println("khoobe ke");
//                        System.out.println(fitnessWeightTable.getText(j));
                        fitnessWeight += fitnessWeightTable.getWeight(j);
                    }
                }
        return fitnessWeight;

    }


    public int getFitnessWeight() {
        return fitnessWeight;
    }

    @Override
    public String toString()
    {
        String s = "";
        for (int i = 0 ; i < 26 ; i ++)
            s += (char)(i + 'A') +":" + (char)(getGene(i) + 'a') + " | ";
        return s;
    }
    public int compareTo(Chromosome other)
    {
        return this.getFitnessWeight() - other.getFitnessWeight();
    }
    public static Comparator<Chromosome> byFitnessWeight = new Comparator<Chromosome>(){


        public int compare(Chromosome o1, Chromosome o2) {
            return o2.compareTo(o1);
        }
    };

    @Override
    public boolean equals(Object obj) {
        Chromosome other = (Chromosome) obj;
        for (int i = 0 ; i < 26 ; i ++)
            if (getGene(i) != other.getGene(i))
                return false;
        return true;


    }
}
