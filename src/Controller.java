import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Created by armin on 3/29/17.
 */
public class Controller {

    private static String code;
    private static int bestFitnessWeight = -1;
    private static Controller controller = null;
    private static Random random = new Random();
    public static Controller getInstance(){
        if (code == null)
            throw  new RuntimeException("code must be set before getInstance");
        if (controller == null) {
            controller = new Controller();

        }

        return controller;
    }
    public static void setCode(String code){
        Controller.code = code;
    }



    private ArrayList<Chromosome> chromosomes;



    private  Controller(){
        System.out.println("Setting up initial chromosomes...");
        chromosomes = new ArrayList<>();
        ArrayList<Integer> permutation = new ArrayList<>();
        for (int i = 0 ; i < 26; i ++)
            permutation.add(i);
        chromosomes.add(new Chromosome(permutation));
        for (int i = 0 ; i < Keys.INIT_POPULATION_SIZE -1 ; i ++)
        {
            Collections.shuffle(permutation);
            insert(new Chromosome(permutation));
            Logger.progress(i,Keys.INIT_POPULATION_SIZE);
        }
        printResult();




    };
    public void search(){
        for (int i = 0; i < Keys.SEARCH_NUMBERS; i ++) {
            if (i% Keys.MUTATION_FREQUENCY == 0)
                crossover();
            else
                mutation();

            checkFittest();
            removeBadChromosomes();
            //removeSameChromosomes();
        }
        printResult();
    }

    private void removeSameChromosomes() {
        for (int i = 0 ; i < chromosomes.size() - 1; i ++)
            if (chromosomes.get(i).equals(chromosomes.get(i + 1)))
                chromosomes.remove(i);
    }


    private void crossover()
    {

        int firstChromosomeIndex = randomSelection(Keys.SELECT_PROBABILITY_CROSSOVER);
        int secondChromosomeIndex = randomSelection(Keys.SELECT_PROBABILITY_CROSSOVER);
        while (firstChromosomeIndex == secondChromosomeIndex)
            secondChromosomeIndex = randomSelection(Keys.SELECT_PROBABILITY_CROSSOVER);
        System.out.println(String.format("crossover selection index : %s , %s" , firstChromosomeIndex,secondChromosomeIndex));
        Chromosome firstChromosome = chromosomes.get(firstChromosomeIndex);
        Chromosome secondChromosome = chromosomes.get(secondChromosomeIndex);

        insert(new Chromosome(firstChromosome,secondChromosome,Keys.CROSSOVER_TYPE));
        insert(new Chromosome(secondChromosome,firstChromosome,Keys.CROSSOVER_TYPE));

        if (randomBoolean(Keys.REMOVE_PARENT_CROSSOVER_PROBABILITY) ) {
            chromosomes.remove(firstChromosomeIndex);
            chromosomes.remove(secondChromosomeIndex);
        }


    }
    private void mutation()
    {
        int randomIndex = randomSelection(Keys.SELECT_PROBABILITY_MUTATION);
        int swapTime = random.nextInt(Keys.SWAP_IN_MUTATION_RANGE);
        System.out.println(String.format("mutation selection index : %s , swap: %s" , randomIndex,swapTime));
        insert(new Chromosome(chromosomes.get(randomIndex),swapTime));
        if (randomBoolean(Keys.REMOVE_PARENT_MUTATION_PROBABILITY) && randomIndex != 0)
            chromosomes.remove(randomIndex);
    }
    private int randomSelection(double p)
    {
        for (int i = 0 ; i < chromosomes.size() ; i ++)
            if (randomBoolean(p))
                return i;
        return 0;

    }
    private void insert(Chromosome chromosome)
    {
        chromosomes.add(chromosome);
        int pointer = chromosomes.size() - 1;
//        System.out.println("new chromosome has fittest weight : " + chromosome.getFitnessWeight() + ", rank: " + pointer);
//        System.out.println("best has: " + chromosomes.get(0).getFitnessWeight());

        while (pointer > 0 && chromosomes.get(pointer).getFitnessWeight() > chromosomes.get(pointer-1).getFitnessWeight()) {
            Collections.swap(chromosomes, pointer, pointer - 1);
            pointer--;
        }
        System.out.println("new chromosome has fittest weight : " + chromosome.getFitnessWeight() + ", rank: " + pointer);
    }
    public void removeBadChromosomes()
    {
        while (chromosomes.size() > Keys.POPULATION_SIZE)
            chromosomes.remove(chromosomes.size()-1);

    }
    private boolean randomBoolean(double p)
    {
        return random.nextDouble() < p;
    }

    private void checkFittest()
    {
        Chromosome fittest = chromosomes.get(0);
        if (fittest.getFitnessWeight() > bestFitnessWeight + 100)
        {
            System.out.println("better chromosome is found. : " + fittest.getFitnessWeight());
            bestFitnessWeight = fittest.getFitnessWeight();
            if (fittest.getFitnessWeight() > Keys.PROPER_FITTEST_WEIGHT) {
                File.writeFileBestChromosome(fittest);
            }
        }
        System.out.println("current best fittest weight : " + fittest.getFitnessWeight());

    }


    public void printResult()
    {
        for (int i = 0 ; i < 10 ; i ++){
            System.out.println("FitnessWeight : " + chromosomes.get(i).getFitnessWeight() + " decodedString : \n" + chromosomes.get(i).decodeString());
        }

    }
    public static String getCode(){
        if (code == null)
            throw new RuntimeException("Code is not set on controller");
        return code;
    }

}
