import java.util.ArrayList;

/**
 * Created by armin on 3/28/17.
 */
public class FitnessWeightTable {
    private ArrayList<FitnessPair> fitnessPairs;
    private static FitnessWeightTable fitnessWeightTable = null;
    public static FitnessWeightTable getInstance(){
        if (fitnessWeightTable == null)
            fitnessWeightTable = new FitnessWeightTable();
        return fitnessWeightTable;
    }
    private FitnessWeightTable(){
        fitnessPairs = new ArrayList<>();
        FitnessPair fitnessPair = new FitnessPair("TH",2);
        fitnessPairs.add(fitnessPair);
        FitnessPair fitnessPair2= new FitnessPair("HE",1);
        fitnessPairs.add(fitnessPair2);
        FitnessPair fitnessPair3 = new FitnessPair("IN",1);
        fitnessPairs.add(fitnessPair3);
        FitnessPair fitnessPair4 = new FitnessPair("IN",1);
        fitnessPairs.add(fitnessPair4);
        FitnessPair fitnessPair5 = new FitnessPair("ER",1);
        fitnessPairs.add(fitnessPair5);
        FitnessPair fitnessPair6 = new FitnessPair("AN",1);
        fitnessPairs.add(fitnessPair6);
        FitnessPair fitnessPair7 = new FitnessPair("ED",1);
        fitnessPairs.add(fitnessPair7);
        FitnessPair fitnessPair8 = new FitnessPair("THE",5);
        fitnessPairs.add(fitnessPair8);
        FitnessPair fitnessPair9 = new FitnessPair("ING",5);
        fitnessPairs.add(fitnessPair9);
        FitnessPair fitnessPair10 = new FitnessPair("AND",5);
        fitnessPairs.add(fitnessPair10);
        FitnessPair fitnessPair11 = new FitnessPair("EEE",-5);
        fitnessPairs.add(fitnessPair11);

        /////
//        fitnessPairs.add(new FitnessPair(" THE " , 20));
//        fitnessPairs.add(new FitnessPair(" I " , 20));
//        fitnessPairs.add(new FitnessPair(" HELLO ", 1000));

    }

    public int getSize(){
        return fitnessPairs.size();
    }
    public String getText(int index){return fitnessPairs.get(index).getText();}
    public int getWeight(int index){return fitnessPairs.get(index).getWeight();}
}
