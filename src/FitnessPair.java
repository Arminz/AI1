/**
 * Created by armin on 3/28/17.
 */
public class FitnessPair {
    private String text;
    private int weight;
    public FitnessPair(String text, int weight)
    {
        this.text = text;
        this.weight = weight;
    }
    public String getText(){return text;}
    public int getWeight(){return weight;}
}
