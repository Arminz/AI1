import java.io.*;

/**
 * Created by armin on 3/29/17.
 */
public class File {
    private static final String CODE_FILE_NAME = "EncryptedText.txt";
    private static final String BEST_CHROMOSOME_FILE_NAME = "Best Chromosome.txt";
    private static final String BEST_UPDATES = "Best Updates";

    public static String readFile() {
        FileReader fr = null;
        try {
            fr = new FileReader(CODE_FILE_NAME);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        BufferedReader br = new BufferedReader(fr);
        String text = "";
        String currentLine;
        try {
            while ((currentLine = br.readLine()) != null)
                text += currentLine + " ";
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            fr.close();
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return text;
    }
    public static void writeFileBestChromosome(Chromosome chromosome)
    {
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(BEST_CHROMOSOME_FILE_NAME,"UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            pw.write("Chromosome: fitness weight: " + chromosome.getFitnessWeight() + "\n");
            pw.write(chromosome.toString() + "\n");
            pw.write(chromosome.decodeString() + "\n");
        }
        finally {
            pw.close();
        }

    }
//    public static void writeFileBestUpdates(String s)
//    {
//        PrintWriter pw = null;
//        try {
//            pw = new PrintWriter(BEST_CHROMOSOME_FILE_NAME,"UTF-8");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        try {
//            pw.write("Chromosome: fitness weight: " + chromosome.getFitnessWeight() + "\n");
//            pw.write(chromosome.toString() + "\n");
//            pw.write(chromosome.decodeString() + "\n");
//        }
//        finally {
//            pw.close();
//        }
//
//    }
}
