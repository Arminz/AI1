/**
 * Created by armin on 3/28/17.
 */
public  class Main {

    public static void main(String[] args)
    {
       Controller.setCode(Input.fromCode());
       Controller controller = Controller.getInstance();
        controller.search();
    }
}